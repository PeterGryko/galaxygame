package pg.galaxy.game;

import java.util.ArrayList;

import pg.galaxy.animation.Animation;
import pg.galaxy.model.BonusType;
import pg.galaxy.model.GameEnemy;
import pg.galaxy.model.GameFence;
import pg.galaxy.model.GameSprait;
import pg.galaxy.model.GameStick;
import pg.galaxy.model.Projectiles;
import pg.galaxy.model.ViewHolder;
import pg.galaxy.spawn.SpawnEnemy;
import pg.galaxy.spawn.SpawnProjectile;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

//This class draw all view visible to the user. Here are touchEvents to. 

public class GameView extends SurfaceView implements SurfaceHolder.Callback {

	private String TAG = "GameView";

	private GameLogic gamelogic;
	private GameSprait mainSprajt;
	private GameStick moveStick;
	private GameStick rotationStick;
	private GameFence gameFence;
	private SpawnEnemy spawnEnemy;
	private SpawnProjectile spawnProjectiles;
	// private Bonus type;

	private boolean shipIsMoving = false;
	private boolean isShooting = false;

	private double score = 0;

	private ViewHolder vh = new ViewHolder(this);
	private BonusType bt = new BonusType(this);

	// enemies and bullets lists.
	private ArrayList<Projectiles> bullets = new ArrayList<Projectiles>();
	private ArrayList<GameEnemy> enemies = new ArrayList<GameEnemy>();
	private ArrayList<Animation> animations = new ArrayList<Animation>();

	private WindowManager wm;
	private Display display;
	private Point size;

	public static int screenWidth;
	public static int screenHeight;

	// rIndex is finger index on right analog stick
	// lIndex is finger index on left analog stick
	private int rIndex = 0;
	private int lIndex = 0;

	Paint paint = new Paint();

	//
	public GameView(Context context) {
		super(context);

		Log.d(TAG, Boolean.toString(this.isHardwareAccelerated()));		
		wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		display = wm.getDefaultDisplay();
		size = new Point();
		display.getSize(size);
		screenWidth = size.x;
		screenHeight = size.y;

		// create game objects, fence, sticks, ship

		// fence is a rectF 1,5 times bigger than device screen size
		gameFence = new GameFence(-(screenWidth / 4), -(screenHeight / 4),
				screenWidth + screenWidth / 4, screenHeight + screenHeight / 4);
		gameFence.setPaint();

		mainSprajt = new GameSprait(vh.getShip(), gameFence.getFence()
				.centerX(), gameFence.getFence().centerY());

		rotationStick = new GameStick(vh.getStick(), vh.getStickTop(),
				screenWidth - vh.getStick().getWidth() - 10, screenHeight
						- vh.getStick().getHeight() - 10);

		moveStick = new GameStick(vh.getStick(), vh.getStickTop(), 10,
				screenHeight - vh.getStick().getHeight() - 10);

		// type = new Bonus(this);
		spawnEnemy = new SpawnEnemy(vh.getEnemyMap(), this);
		spawnProjectiles = new SpawnProjectile(this);

		// main game loop object.
		gamelogic = new GameLogic(getHolder(), this);

		setFocusable(true);

		getHolder().addCallback(this);

		spawnEnemy.setX(mainSprajt.getX());
		spawnEnemy.setY(mainSprajt.getY());
		spawnEnemy.setFence(gameFence.getFence());
		spawnEnemy.start();

	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {

		gamelogic.setGameState(GameLogic.RUNNING);
		gamelogic.start();

	}

	public void setGameState(int state) {
		gamelogic.setGameState(state);
		spawnEnemy.stopLoop();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
	}

	public double getScore() {
		return score;
	}

	public BonusType getBonusType() {
		return bt;
	}

	// method add new projectiles to list
	public void addBullet(Projectiles p) {
		bullets.add(p);
	}

	// method add new enemie to the list
	public void addEnemy(GameEnemy e) {
		enemies.add(e);
	}

	// method remove enemy from the list
	public void removeEnemy(GameEnemy e) {
		e.destroy();
		Log.d(TAG, Integer.toString(enemies.size()));
		enemies.remove(e);

	}

	public void addAnimation(Animation a) {
		animations.add(a);
	}

	public void removeAnimation(Animation a) {
		animations.remove(a);
	}

	// public int getType() {
	// type.estimateType(score);
	// / return type.getType();
	// }

	// method work when we release finger from analog stick
	protected void releaseFinger(MotionEvent event) {
		// if release right stick top and zero right finger index

		if (event.getPointerId(event.getActionIndex()) + 1 == rIndex) {

			rotationStick.setXYtop();
			rIndex = 0;
			Log.d(TAG, "rotation stick centered");
			isShooting = false;

		}

		// if release left stick ship stops, left stick centers and index sets
		// to 0
		else if (event.getPointerId(event.getActionIndex()) + 1 == lIndex) {
			moveStick.setXYtop();
			mainSprajt.setMove(0, 0);
			lIndex = 0;
			shipIsMoving = false;

		}
	}

	// when touch the screen
	protected void putFinger(MotionEvent event) {
		// in loop we check each finger on the screen
		for (int i = 0; i < event.getPointerCount(); ++i) {
			if (event.getPointerId(i) >= 0) {
				// check if left analog is being touch
				if (moveStick.distanceFC(vh.getStick(), event.getX(i),
						event.getY(i), 10)) {

					// if set lIndex
					lIndex = event.getPointerId(i) + 1;

					// estimate angle beetween analog center and finger
					moveStick.setDirection(event.getX(i), event.getY(i),
							vh.getShipSpeed());
					// based on angle, we set current ship direction
					mainSprajt.setMoveX((float) moveStick.getMoveX());
					mainSprajt.setMoveY(-(float) moveStick.getMoveY());
					shipIsMoving = true;

				}
				// if on right stick
				if (rotationStick.distanceFC(vh.getStick(), event.getX(i),
						event.getY(i), 10)) {
					isShooting = true;
					// set right finger id
					rIndex = event.getPointerId(i) + 1;
					// estimate ship rotation based on angle between finger and
					// stick center
					mainSprajt.setRotation(rotationStick.estimateRotation(
							event.getX(i), event.getY(i), screenWidth
									- vh.getStick().getWidth() / 2 - 10,
							screenHeight - vh.getStick().getHeight() / 2 - 10));

					// based on stick angle, we estimate shooting direction
					rotationStick.setDirection(event.getX(i), event.getY(i),
							vh.getBulletSpeed());

				}

			}

		}
	}

	// touch events
	public boolean onTouchEvent(MotionEvent event) {

		switch (event.getActionMasked()) {

		case MotionEvent.ACTION_DOWN:
		case MotionEvent.ACTION_POINTER_DOWN:
		case MotionEvent.ACTION_MOVE: {

			putFinger(event);

		}
			break;

		case MotionEvent.ACTION_POINTER_UP: {

			releaseFinger(event);
		}
			break;

		case MotionEvent.ACTION_UP: {
			releaseFinger(event);
		}
			break;

		}

		return true;

	}

	// draw everything on the screen
	public void onDraw(Canvas canvas) {

		try {

			canvas.drawColor(Color.BLACK);
			moveStick.draw(canvas);
			rotationStick.draw(canvas);
			mainSprajt.draw(canvas);

			gameFence.drawFence(canvas);

			paint.setColor(Color.RED);
			canvas.drawText("Score: " + Double.toString(score), 70, 50, paint);
			// in loop draw all bullets
			if (bullets != null)

				for (int i = 0; i < bullets.size(); i++) {
					if (bullets.get(i) != null)
						bullets.get(i).draw(canvas);
				}
			// in loop draw all enemies
			if (enemies != null)
				for (int i = 0; i < enemies.size(); i++)
					enemies.get(i).draw(canvas);
			if (animations != null)
				for (int i = 0; i < animations.size(); i++)
					animations.get(i).draw(canvas);

		} catch (NullPointerException e) {
		}

	}

	// method refreash screen elements
	public void update() {

		if (isShooting) {
			spawnProjectiles.spawn(mainSprajt.getX(), mainSprajt.getY(),
					(float) rotationStick.getMoveX(),
					-(float) rotationStick.getMoveY(),
					System.currentTimeMillis());
		}

		// check sprajts bounds
		checkBounds();
		// update ship position
		mainSprajt.update();
		// check for bonuses;
		bt.checkTime(System.currentTimeMillis());

		if (animations != null) {
			for (int i = 0; i < animations.size(); i++)
				animations.get(i).update(System.currentTimeMillis());
		}

		// if ship is moving, also fence is moving
		if (shipIsMoving) {
			gameFence.updateRect((float) mainSprajt.getMoveX(),
					(float) mainSprajt.getMoveY());
			if (enemies != null) {
				for (int i = 0; i < enemies.size(); i++) {
					enemies.get(i).updateAccToFence(
							(int) mainSprajt.getMoveX(),
							(int) mainSprajt.getMoveY());
				}
			}

			if (animations != null) {
				for (int i = 0; i < animations.size(); i++)
					animations.get(i).updateAccToFence(
							(int) mainSprajt.getMoveX(),
							(int) mainSprajt.getMoveY());
			}

		}

		// estimate enemies direction
		if (enemies != null) {
			for (int j = 0; j < enemies.size(); j++) {
				enemies.get(j).setDirection(mainSprajt.getX(),
						mainSprajt.getY());
			}
		}

	}

	// method check if everything is inside the fence
	public void checkBounds() {
		// get current fence coordinates
		RectF f = gameFence.getFence();

		if (mainSprajt.getX() >= f.right)
			mainSprajt.setMoveX(-3);
		if (mainSprajt.getY() >= f.bottom)
			mainSprajt.setMoveY(-3);
		if (mainSprajt.getX() <= f.left)
			mainSprajt.setMoveX(3);
		if (mainSprajt.getY() <= f.top)
			mainSprajt.setMoveY(3);

		checkAnimations();
		checkEnemy(f);
		checkBullets(f);

	}

	// method checks if enemies are inside the fence
	private void checkEnemy(RectF f) {
		if (enemies != null) {
			for (int i = 0; i < enemies.size(); i++) {
				enemies.get(i).update();

				if (mainSprajt.checkCollision(enemies.get(i).getX(), enemies
						.get(i).getY(), vh.getShip()))
					gamelogic.setGameState(0);

				if (enemies.get(i).getX() > f.right
						|| enemies.get(i).getX() < f.left
						|| enemies.get(i).getY() > f.bottom
						|| enemies.get(i).getY() < f.top) {
					enemies.get(i).destroy();
					enemies.remove(i);

				}
			}
		}

	}

	// check collision with animation
	public void checkAnimations() {
		if (animations != null) {
			for (int i = 0; i < animations.size(); i++) {
				if (animations.get(i).checkCollision(mainSprajt.getX(),
						mainSprajt.getY(), mainSprajt.getBitmap())) {

					bt.setBonus(System.currentTimeMillis());

					Animation a = new Animation(vh.getBonusDis(), 25,
							animations.get(i).getX(), animations.get(i).getY(),
							30, this);
					animations.remove(i);
					animations.add(a);
					// animations.
				}
			}
		}

	}

	// check if bullets are inside the fence. If outside, they are removed from
	// the list
	// Here ale also check enemies hits.

	private void checkBullets(RectF f) {
		if (bullets != null) {

			for (int i = 0; i < bullets.size(); i++) {
				bullets.get(i).update();

				if (bullets.get(i).getX() > f.right
						|| bullets.get(i).getY() > f.bottom
						|| bullets.get(i).getX() < f.left
						|| bullets.get(i).getY() < f.top) {
					Animation animation = new Animation(vh.getSplash2(), 12,
							(int) bullets.get(i).getX(), (int) bullets.get(i)
									.getY(), 30, this);

					this.addAnimation(animation);
					bullets.remove(i);
					continue;
				}

				for (int j = 0; j < enemies.size(); j++) {

					if (enemies.get(j).checkCollision(bullets.get(i), null)) {

						// id enemie is destoyed, increase the score
						if (enemies.get(j).destroy()) {
							Bonus bonus = new Bonus(vh.getSpaceCore(), 71,
									(int) enemies.get(j).getX(), (int) enemies
											.get(j).getY(), 30, this);
							bonus.spawnBonus();

							Animation animation = null;

							if (!bt.getBonus())
								animation = new Animation(vh.getSplash(), 20,
										(int) enemies.get(j).getX(),
										(int) enemies.get(j).getY(), 30, this);
							else
								animation = new Animation(vh.getGreenSplash(),
										25, (int) bullets.get(i).getX(),
										(int) bullets.get(i).getY(), 30, this);

							animations.add(animation);
							enemies.remove(j);
							score += 13.47;
						}
						bullets.remove(i);
						break;

					}
				}

			}
		}
	}

}
