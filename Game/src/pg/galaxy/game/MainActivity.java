package pg.galaxy.game;

import android.os.Bundle;
import android.util.Log;
import android.app.Activity;

public class MainActivity extends Activity {

	private String TAG = "MainActivity";
	private GameView view;

	// private GameLogic logic = new GameLogic();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		view = new GameView(this);
		getActionBar().hide();
		setContentView(view);
	}

	public void onBackPressed() {
		Log.d(TAG, "backpressed");
		view.setGameState(0);
		this.finish();
	}

}
