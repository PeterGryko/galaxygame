package pg.galaxy.game;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class GameLogic extends Thread {

	//this is main game loop. Loop is active according to INT flag. Between eatch steep
	//loop waits 30ms (30fps), refreash  (update sprites, enemies, sticks) and redraw view
	


	private String TAG = "GameLogic";
	private int game_state;
	public static final int PAUSED = 0;
	public static final int READY = 1;
	public static final int RUNNING = 2;
	private GameView gameview;
	private SurfaceHolder surfaceholder;

	public GameLogic(SurfaceHolder surfaceholder, GameView gameview) {
		super();
		this.gameview = gameview;
		this.surfaceholder = surfaceholder;

	}

	public int getGameState() {
		return game_state;
	}

	public void setGameState(int gamestate) {
		this.game_state = gamestate;
	}

	public void run() {
		Canvas canvas;

		while (game_state == RUNNING) {
			canvas = null;
			try {
				canvas = this.surfaceholder.lockCanvas();
				synchronized (surfaceholder) {
					try {
						Thread.sleep(30);
					} catch (InterruptedException e) {
					}

					gameview.update();
					gameview.onDraw(canvas);

				}

			} finally {
				if (canvas != null) {
					this.surfaceholder.unlockCanvasAndPost(canvas);

				}

			}

		}

	}

}
