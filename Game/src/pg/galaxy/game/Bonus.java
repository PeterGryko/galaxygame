package pg.galaxy.game;

import pg.galaxy.animation.Animation;
import android.graphics.Bitmap;
import android.graphics.Rect;

public class Bonus extends Animation {

	private double score;
	private Bitmap bitmap;
	private int type;
	private GameView view;
	private Animation animation;
	private Rect r;
	private long currentTime;
	private long delay;

	public Bonus(Bitmap bitmap, int nrFrames, int x, int y, int fps,
			GameView view) {
		super(bitmap, nrFrames, x, y, fps, view);
		this.view = view;
	}


	public void spawnBonus() {

		if ((Math.random() * 100) > 90)
			view.addAnimation(this);
	}

	public void update(long gametime) {
		if (gametime > this.getFrameTicker() + this.getFramePeriod()) {
			this.setFrameTicker(gametime);

			this.setCurrentFrame(this.getCurrentFrame() + 1);

			if (this.getCurrentFrame() >= this.getFramesNumber()) {
				{
					this.setCurrentFrame(0);

				}

			}
			r = this.getSourceRect();
			r.left = this.getCurrentFrame() * this.getSpriteWidth();
			r.right = r.left + this.getSpriteWidth();
			this.setSourceRect(r);
		}

	}

	public boolean checkCollision(float eX, float eY, Bitmap bitmap) {
		double distance = Math.sqrt((this.getX() - eX) * (this.getX() - eX)
				+ (this.getY() - eY) * (this.getY() - eY));

		if (bitmap != null) {
			if (distance < this.getSpriteWidth() / 2 + bitmap.getWidth() / 2) {
				// Log.d(TAG, "collision detected!");
				return true;
			}
		}
		return false;
	}

}
