package pg.galaxy.animation;

import pg.galaxy.game.GameView;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

public class Animation {

	private Bitmap bitmap;
	private Rect sourceRect;
	private int nrFrames;
	private int currentFrame;
	private long frameTicker;
	private long framePeriod;
    private GameView view;
	

	private int spriteWidth;
	private int spriteHeight;
	private int x;
	private int y;

	public Animation(Bitmap bitmap, int nrFrames,int x, int y,int fps, GameView view) {
		this.nrFrames = nrFrames;
		this.bitmap = bitmap;
		
		this.view=view;

		spriteWidth = bitmap.getWidth() / nrFrames;
		spriteHeight = bitmap.getHeight();
		sourceRect = new Rect(0, 0, spriteWidth, spriteHeight);
		framePeriod = 1000/fps; //dlugosc jednej klatki (animation fps)
		frameTicker = 01;

		this.x=x-spriteWidth/2;
		this.y=y-spriteHeight/2;
	}

	

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public GameView getView()
	{return view;}
	
	public Bitmap getBitmap()
	{return bitmap;}
	
	public Rect getSourceRect()
	{return sourceRect;}
	
	public void setSourceRect(Rect sourceRect)
	{this.sourceRect=sourceRect;}
	
	public int getFramesNumber()
	{return nrFrames;}
	
	public void setCurrentFrame(int currentFrame)
	{this.currentFrame=currentFrame;}
	
	public int getCurrentFrame()
	{return currentFrame;}
	
    public void increaseCurrentFrame()
    {currentFrame++;}
	
	public long getFrameTicker()
	{return frameTicker;}
	
	public void setFrameTicker(long frameTicker)
	{this.frameTicker=frameTicker;}
	
	public long getFramePeriod()
	{return framePeriod;}
	
	public int getSpriteWidth()
	{return spriteWidth;}
	
	public int getSpriteHeight()
	{return spriteHeight;}
	
	public int getX()
	{return x;}
	
	public int getY()
	{return y;}

	public void update(long gametime) {
		if (gametime > frameTicker+framePeriod) {
			frameTicker = gametime;

			currentFrame++;
			if (currentFrame >= nrFrames) {
				{
					currentFrame = 0;
                    view.removeAnimation(this);
					
				}

			}
			sourceRect.left = currentFrame * spriteWidth;
			sourceRect.right = sourceRect.left + spriteWidth;
		}

	}
	
	// ta metoda przesowa przecinikow odnosząc sie do ogrodzenia planszy
	public void updateAccToFence(float xu, float yu) {
		this.setX(x - (int)xu);
		this.setY(y - (int)yu);
	}

	public void draw(Canvas canvas) {
		Rect dest = new Rect(x, y, x + spriteWidth, y + spriteHeight);
		canvas.drawBitmap(bitmap, sourceRect, dest, null);
	}

	public boolean checkCollision(float eX, float eY, Bitmap bitmap)
	{return false;}
	
}
