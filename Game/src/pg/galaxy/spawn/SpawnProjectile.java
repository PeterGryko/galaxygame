package pg.galaxy.spawn;

import java.util.ArrayList;

import pg.galaxy.game.GameView;
import pg.galaxy.game.R;
import pg.galaxy.model.Projectiles;

//class describes projectiles 
public class SpawnProjectile {
	// x,y are projectiles spawn coordinates, xm,ym are directions

	private GameView view;
	private int delay;
	// private int type;
	private long lastTime = 0;

	//
	public SpawnProjectile(GameView view) {

		this.view = view;
		// type = view.getType();
		// if (view.getType() < 2)
		delay = view.getResources().getInteger(R.integer.bullet_delay_type1);
		// else
		// delay = delay = view.getResources().getInteger(
		// / R.integer.bullet_delay_type2);
	}

	// starts thead
	public void spawn(float x, float y, float xm, float ym, long time) {

		// create new bullet and estimate flight direction

		if (time > lastTime + delay) {

			if (!view.getBonusType().getBonus()) {
				lastTime = time;
				Projectiles p = new Projectiles(x, y, view);
				p.setMove(xm, ym);
				view.addBullet(p);
			} else {
				lastTime = time;
				Projectiles p = new Projectiles(x, y, view);
				Projectiles p1 = new Projectiles(x, y, view);
				Projectiles p2 = new Projectiles(x, y, view);
				p.setMove(xm, ym);
				p1.setMove(xm - 2, ym - 2);
				p2.setMove(xm + 2, ym + 2);
				view.addBullet(p);
				view.addBullet(p1);
				view.addBullet(p2);
			}

		}

	}
}
