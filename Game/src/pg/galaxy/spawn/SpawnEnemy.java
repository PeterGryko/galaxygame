package pg.galaxy.spawn;

import java.util.ArrayList;
import java.util.Random;

import pg.galaxy.game.GameView;
import pg.galaxy.game.R;
import pg.galaxy.model.GameEnemy;

import android.graphics.Bitmap;
import android.graphics.RectF;
import android.util.Log;

//class spawn enemies
public class SpawnEnemy extends Thread {

	private String TAG = "SpawnEnemy";

	private Bitmap bitmap;
	private float shipX;
	private float shipY;
	private GameView view;
	private RectF fence;
	private long delay;
	private float enemySpeed;
	private boolean flag = true;

	//
	public SpawnEnemy(Bitmap bitmap, GameView view) {
		this.bitmap = bitmap;

		this.view = view;

		delay = view.getResources().getInteger(R.integer.enemy_delay_type1);

	}

	public void setX(float x) {
		shipX = x;
	}

	public void setY(float y) {
		shipY = y;
	}

	public void setFence(RectF fence) {
		this.fence = fence;
	}

	// spawn enemy in random fence corner
	private GameEnemy createEnemy() {

		int index = new Random().nextInt(4);
		GameEnemy enemy = null;

		if (index == 0) {
			enemy = new GameEnemy(bitmap, (int) (fence.left + 20),
					(int) (Math.random() * (fence.bottom - fence.top)), view);
		} else if (index == 1) {
			enemy = new GameEnemy(bitmap,
					(int) (Math.random() * (fence.right - fence.left)),
					(int) (fence.bottom - 20), view);
		} else if (index == 2) {
			enemy = new GameEnemy(bitmap, (int) (fence.right - 20),
					(int) (Math.random() * (fence.bottom - fence.top)), view);
		} else if (index == 3) {
			enemy = new GameEnemy(bitmap,
					(int) (Math.random() * fence.right - fence.left),
					(int) (fence.top + 20), view);
		}

		return enemy;

	}

	// count - enemies number, delay- delay between spawns
	public void run() {

		while (flag) {
			try {
			//	view.setEnemiesFlag(true);

				Thread.sleep(delay);

				int count = 0;

				while (count < 6) {
					GameEnemy enemy = createEnemy();
					enemy.setDirection(shipX, shipY);
					view.addEnemy(enemy);

					Log.d(TAG, "enemy spawned!");
					count++;
				}
			//	view.setEnemiesFlag(false);

			} catch (Throwable t) {
			}
		}
	}

	public void stopLoop()
	{flag = false;
	 Thread.currentThread().interrupt();
	// Thread.currentThread().stop();
	}

}
