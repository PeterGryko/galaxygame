package pg.galaxy.model;

import pg.galaxy.game.GameView;
import pg.galaxy.game.R;

public class BonusType {

	private GameView view;
	private long delay = 0;
	private long initTime = 0;

	public BonusType(GameView view) {
		this.view = view;
		delay = view.getResources().getInteger(R.integer.bonus_time);
	}

	private boolean flag = false;

	public boolean getBonus() {
		return flag;
	}

	public void setBonus(long initTime) {
		flag = true;
		this.initTime = initTime;
	}

	public void checkTime(long time) {
		if (flag) {
			if (time > initTime + delay) {
				flag = false;

			}

		}
	}
}
