package pg.galaxy.model;

import android.graphics.Bitmap;
import android.graphics.Canvas;

public class Sprait {

	public float x;
	public float y;
	public double move_x;
	public double move_y;
	public Bitmap bitmap;

	

	public void setX(float x) {
		this.x = x;
	}

	public void setY(float y) {
		this.y = y;
	}

	public void setMove(float move_x, float move_y) {
		this.move_x = move_x;
		this.move_y = move_y;
	}

	public void setMoveX(float move_x) {
		this.move_x = move_x;
	}

	public void setMoveY(float move_y) {
		this.move_y = move_y;
	}

	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public double getMoveX() {
		return move_x;
	}

	public double getMoveY() {
		return move_y;
	}

	public Bitmap getBitmap() {
		return bitmap;
	}
	
	public int getWidth()
	{return bitmap.getWidth();}
	
	public int getHeight()
	{return bitmap.getHeight();}

	public void update() {
	}

	public void draw(Canvas canvas) {
	}

}
