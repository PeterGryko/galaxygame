package pg.galaxy.model;

import pg.galaxy.game.GameView;
import pg.galaxy.game.R;
import android.graphics.Bitmap;
import android.util.Log;

//class enemy

public class GameEnemy extends GameSprait {

	private String TAG = "GameEnemy";
	private int i = 1;
	private float moveAngle;
	// private double moveX;
	// private double moveY;
	private double tan;
	private GameView view;
	private int count = 0;
	private int destroyCount = 0;
	private int rotateAngle = 10;
	private float speed;
	private int type;

	private boolean isDestroyed = false;

	// private Animation animation;

	public GameEnemy(Bitmap bitmap, int x, int y, GameView view) {
		super(bitmap, x, y);
		this.view = view;
		// type = view.getType();
		// if (type < 2)
		speed = view.getResources().getDimension(R.dimen.enemy_speed_type1);
		// else
		// speed = view.getResources().getDimension(R.dimen.enemy_speed_type2);

	}

	// method estimates angle between ship and enemy
	public float estimateRotation(float x, float y) {

		float rotation = (float) Math.toDegrees(Math.atan2((x - this.getX()),
				-(y - this.getY())));

		if (rotation < 0)
			rotation += 360;

		return rotation;

	}

	// parent method super + permanent enemy rotation
	public void update() {
		super.update();
		if (i <= -360) {
			i = 0;
			if (rotateAngle < 50)
				rotateAngle += 5;
		}

		i -= rotateAngle;
		this.setRotation(i);

	}

	// method move enemies according to fence
	public void updateAccToFence(float x, float y) {
		this.setX(this.getX() - x);
		this.setY(this.getY() - y);
	}

	// Medhod estimated angle between ship and enemy, based on this angle
	// enemy current direction is set
	public void setDirection(float x, float y) {

		moveAngle = estimateRotation(x, y);
		// right top
		if (moveAngle > 0 || moveAngle <= 90) {
			tan = Math.sin(Math.toRadians(moveAngle));
			move_x = (speed * tan);

			move_y = Math.sqrt(speed * speed - move_x * move_x);

		}
		// right bottom
		if (moveAngle > 90 && moveAngle <= 180) {

			tan = Math.cos(Math.toRadians(moveAngle - 90));

			move_x = (tan * speed);
			move_y = Math.sqrt(speed * speed - move_x * move_x);
			move_y = -move_y;

		}
		// left bottom
		if (moveAngle > 180 && moveAngle <= 270) {
			tan = Math.sin(Math.toRadians(moveAngle - 180));
			move_x = (tan * speed);
			move_y = Math.sqrt(speed * speed - move_x * move_x);

			move_x = -move_x;
			move_y = -move_y;

		}
		// left top
		if (moveAngle > 270 && moveAngle <= 360) {

			tan = Math.cos(Math.toRadians(moveAngle - 270));

			move_x = (tan * speed);
			move_y = Math.sqrt(speed * speed - move_x * move_x);

			move_x = -move_x;

		}

		this.setMoveX((float) move_x);
		this.setMoveY(-(float) move_y);

	}

	// if enemy is hitted, increease the count. If count is equal to constatnt,
	// retrn true
	public boolean destroy() {

		if (type > 1)
			return true;

		else if (count >= 1) {
			Log.d(TAG, "enemy object destroyed");
			isDestroyed = true;
			return true;

		} else
			count++;

		return false;

	}

	// check if enemy is hitted. If distance between enemy center and bullet is
	// smaller
	// than enemy bitmap/2
	public boolean checkCollision(Projectiles projectile, Bitmap enemyBitmap) {

		float eX = projectile.getX();
		float eY = projectile.getY();

		double distance = Math.sqrt((getX() - eX) * (getX() - eX)
				+ (getY() - eY) * (getY() - eY));

		if (distance < this.getBitmap().getWidth() / 2) {
			Log.d(TAG, "enemy Hitteddd!!! cryticall!!");

			return true;
		}
		return false;
	}

}
