package pg.galaxy.model;

import pg.galaxy.game.GameView;
import pg.galaxy.game.R;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class ViewHolder {

	private GameView view;
	private int shipSize;
	private int enemySize;
	private int stickSize;

	private Bitmap ship;
	private Bitmap stick;
	private Bitmap stickTop;
	private Bitmap enemyMap;
	private Bitmap splash;
	private Bitmap splash2;
	private Bitmap spaceCore;
	private Bitmap bonusDis;
	private Bitmap greenSplash;
	private Bitmap greenSplash2;

	private float shipSpeed;
	private float bulletSpeed;

	public ViewHolder(GameView view) {
		this.view = view;
		init();
	}

	private void init() {
		shipSize = (int) view.getResources().getDimension(R.dimen.ship_size);

		enemySize = (int) view.getResources().getDimension(R.dimen.enemy_size);
		stickSize = (int) view.getResources().getDimension(R.dimen.stick_size);

		ship = Bitmap
				.createScaledBitmap(BitmapFactory.decodeResource(
						view.getResources(), R.drawable.main), shipSize,
						shipSize, true);

		stick = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
				view.getResources(), R.drawable.stick), stickSize, stickSize,
				true);

		stickTop = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
				view.getResources(), R.drawable.sticktop), stickSize / 2,
				stickSize / 2, true);

		enemyMap = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
				view.getResources(), R.drawable.enemy), enemySize, enemySize,
				true);

		splash = BitmapFactory.decodeResource(view.getResources(),
				R.drawable.splash);

		splash2 = BitmapFactory.decodeResource(view.getResources(),
				R.drawable.bullet_type2);
		spaceCore = BitmapFactory.decodeResource(view.getResources(),
				R.drawable.space_core);

		bonusDis = BitmapFactory.decodeResource(view.getResources(),
				R.drawable.bonus_dis);
		greenSplash = BitmapFactory.decodeResource(view.getResources(),
				R.drawable.green_splash2);
		greenSplash2 =BitmapFactory.decodeResource(view.getResources(),
				R.drawable.green_splash3);

		shipSpeed = view.getResources().getDimension(R.dimen.speed);
		bulletSpeed = view.getResources().getDimension(R.dimen.bullet_speed);
	}

	public int getShipSize() {
		return shipSize;
	}

	public int getEnemySize() {
		return enemySize;
	}

	public int getStickSize() {
		return stickSize;
	}

	public Bitmap getShip() {
		return ship;
	}

	public Bitmap getStick() {
		return stick;
	}

	public Bitmap getStickTop() {
		return stickTop;
	}

	public Bitmap getEnemyMap() {
		return enemyMap;
	}

	public Bitmap getSplash() {
		return splash;
	}

	public Bitmap getSplash2() {
		return splash2;
	}

	public Bitmap getSpaceCore() {
		return spaceCore;
	}

	public Bitmap getBonusDis() {
		return bonusDis;
	}

	public Bitmap getGreenSplash() {
		return greenSplash;
	}
	
	public Bitmap getGreenSplash2()
	{return greenSplash2;}

	public float getBulletSpeed() {
		return bulletSpeed;
	}

	public float getShipSpeed() {
		return shipSpeed;
	}
}
