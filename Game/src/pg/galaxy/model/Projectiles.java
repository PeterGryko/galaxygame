package pg.galaxy.model;

import java.util.ArrayList;

import pg.galaxy.game.GameView;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

//class describes projectiles
public class Projectiles extends Sprait {

	private int bulletSize;
	// private int type = 0;
	private Paint paint = new Paint();

	public Projectiles(float x, float y, GameView view) {
		this.x = x;
		this.y = y;

		// type = view.getType();

		// if (type == 1) {
		bulletSize = 4;
		if (!view.getBonusType().getBonus())
			paint.setColor(Color.YELLOW);

		else {
			paint.setColor(Color.CYAN);
		}

		// } else if (type == 2) {

		// paint.setColor(Color.BLUE);
		// bulletSize = 6;
		// } else if (type == 3) {

		// paint.setColor(Color.CYAN);
		// bulletSize = 6;
		// } else if (type == 4) {
		// paint.setColor(Color.GREEN);
		// bulletSize = 6;
		// }

	}

	// update bullet on screen
	public void update() {
		x += move_x;
		y += move_y;

	}

	// draw bullet
	public void draw(Canvas canvas) {

		canvas.drawCircle(x, y, bulletSize, paint);

	}

}
