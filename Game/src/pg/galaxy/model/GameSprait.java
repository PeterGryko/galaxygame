package pg.galaxy.model;

import android.graphics.Bitmap;
import android.graphics.Canvas;

//our ship

public class GameSprait extends Sprait {

	private String TAG = "GameSprajt";

	private float rotation = 0;

	public GameSprait(Bitmap bitmap, float x, float y) {
		this.bitmap = bitmap;
		this.x = x;
		this.y = y;

	}

	// sets,gets

	// method sets current ship rotation angle
	public void setRotation(float rotation) {
		this.rotation = rotation;
	}

	// rotate ship according to rotationStick lean
	public void draw(Canvas canvas) {
		canvas.save();
		canvas.rotate(rotation, x, y);
		canvas.drawBitmap(bitmap, x - (bitmap.getWidth() / 2),
				y - (bitmap.getHeight() / 2), null);

		canvas.restore();

	}

	// move out ship
	public void update() {
		x += (move_x);
		y += (move_y);

	}

	// method checks collision between ship and enemy

	public boolean checkCollision(float eX, float eY, Bitmap enemyBitmap) {
		double distance = Math.sqrt((x - eX) * (x - eX) + (y - eY) * (y - eY));

		if (enemyBitmap != null) {
			if (distance < bitmap.getWidth() / 2 + enemyBitmap.getWidth() / 2) {
				// Log.d(TAG, "collision detected!");
			
				return true;
			}
		}
		return false;
	}

}
