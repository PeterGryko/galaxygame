package pg.galaxy.model;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

//class describes game fence

//fence is moving according to ship .
//enemies are moving with fence

public class GameFence {

	private RectF fence;
	private Paint paint;
	private String TAG = "GameFence";

	public GameFence(int left, int top, int right, int bottom) {
		fence = new RectF(left, top, right, bottom);

	}

	// set fence color, stroke means that we draw only border of rectange
	public void setPaint() {
		paint = new Paint();
		paint.setColor(Color.BLUE);
		paint.setStyle(Paint.Style.STROKE);
	}

	// move fence according to current ship moving
	public void updateRect(float x, float y) {

		fence.right = fence.right - x;
		fence.left = fence.left - x;
		fence.top = fence.top - y;
		fence.bottom = fence.bottom - y;

	
	}

	// return fence
	public RectF getFence() {
		return fence;
	}

	// draw fence
	public void drawFence(Canvas canvas) {

		canvas.drawRect(fence, paint);

	}

}
