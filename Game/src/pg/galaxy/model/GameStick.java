package pg.galaxy.model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class GameStick extends Sprait {

	private float xTop = 0;
	private float yTop = 0;
	private double moveX;
	private double moveY;
	private double tan;

	private float moveAngle = 45;

	private Paint paint = new Paint();

	private Bitmap bitmapTop;

	// arguments are bitmaps and center coordinates
	public GameStick(Bitmap bitmap, Bitmap bitmapTop, int xs, int ys) {

		this.bitmap = bitmap;
		this.bitmapTop = bitmapTop;
		xTop = x = xs + bitmap.getWidth() / 2;
		yTop = y = ys + bitmap.getHeight() / 2;

	}

	// method checks if finger is on stick
	public boolean distanceFC(Bitmap bitmap, float xs, float ys, int i) {
		if (Math.sqrt((double) (x - xs) * (x - xs) + (y - ys) * (y - ys)) < bitmap
				.getWidth() / 2 + i)
			return true;

		else
			return false;

	}

	// method sets top part of the stick
	public void setXYtop() {
		xTop = x;
		yTop = y;
	}

	public double getMoveX() {
		return moveX;
	}

	public double getMoveY() {
		return moveY;
	}

	public void draw(Canvas canvas) {

		canvas.drawBitmap(bitmap, x - bitmap.getWidth() / 2,
				y - bitmap.getHeight() / 2, null);
		canvas.drawBitmap(bitmapTop, xTop - bitmapTop.getWidth() / 2, yTop
				- bitmapTop.getHeight() / 2, null);

	}

	// method estimates direction of moving/shooting
	public void setDirection(float xs, float ys, float a) {

		

		moveAngle = estimateRotation(xs, ys, x, y);
		// right top
		if (moveAngle > 0 || moveAngle <= 90) {
			tan = Math.sin(Math.toRadians(moveAngle));
			moveX = (a * tan);

			moveY = Math.sqrt(a * a - moveX * moveX);

		}
		// right bottom
		if (moveAngle > 90 && moveAngle <= 180) {

			tan = Math.cos(Math.toRadians(moveAngle - 90));

			moveX = (tan * a);
			moveY = Math.sqrt(a * a - moveX * moveX);
			moveY = -moveY;

		}
		// left bottom
		if (moveAngle > 180 && moveAngle <= 270) {
			tan = Math.sin(Math.toRadians(moveAngle - 180));
			moveX = (tan * a);
			moveY = Math.sqrt(a * a - moveX * moveX);

			moveX = -moveX;
			moveY = -moveY;

		}
		// left top
		if (moveAngle > 270 && moveAngle <= 360) {

			tan = Math.cos(Math.toRadians(moveAngle - 270));

			moveX = (tan * a);
			moveY = Math.sqrt(a * a - moveX * moveX);

			moveX = -moveX;

		}

	}

	// angle between two points
	public float estimateRotation(float x, float y, float xs, float ys) {

		xTop = (int) x;
		yTop = (int) y;

		float rotation = (float) Math.toDegrees(Math.atan2(x - xs, -(y - ys)));

		if (rotation < 0)
			rotation += 360;

		return rotation;

	}

}
